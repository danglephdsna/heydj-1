#!/usr/bin/python
# -*- coding: utf-8 -*-

from datetime import datetime
from flask import Flask, make_response, request, redirect

import sys, os, time, json

import RPi.GPIO as GPIO


app = Flask(__name__)

@app.route("/")
def home():
    return json.dumps({'title':'welcome to Sesami'})


@app.route("/opendoor")
def opendoor():
    GPIO.output(17,True)
    time.sleep(1)
    GPIO.output(17,False)
    return json.dumps({'status':'door is opened'})


if __name__ == '__main__':

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(17, GPIO.OUT)
    app.run(debug=True, threaded=False, host="0.0.0.0")
