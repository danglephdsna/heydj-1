#!/usr/bin/python
# -*- coding: utf-8 -*-

import os,sys

from multiprocessing import Pool


def run_process(process):
    os.system('python {}'.format(process))

if __name__ == '__main__':
    os.chdir(os.path.dirname(os.path.abspath(__file__)))

    processes = ('webserver.py','botserver.py')


    pool = Pool(processes=2)

    try:
        pool.map(run_process, processes)
    except KeyboardInterrupt:
        pool.terminate()
        pool.close()
        pool.join()
        sys.exit()
    except Exception as e:
        pool.terminate()
    finally:
        pool.join()



