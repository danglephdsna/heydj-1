#!/usr/bin/env python
import RPi.GPIO as GPIO
import requests

pin=27

GPIO.setmode(GPIO.BCM)
GPIO.setup(pin, GPIO.IN , pull_up_down=GPIO.PUD_DOWN)


def gpio_callback(pinno):
    return requests.get("http://10.2.13.146:5000/bellrang")

#GPIO.add_event_callback(pin, gpio_callback)
#GPIO.add_event_detect(pin, GPIO.RISING, callback=gpio_callback, bouncetime=300)

loop=True
while loop:
    try:
        print("waiting for the bell")
        channel = GPIO.wait_for_edge(pin, GPIO.RISING)
        if channel is not None:
            gpio_callback(pin)
    except KeyboardInterrupt:
        loop=False
        GPIO.cleanup()
GPIO.cleanup()
