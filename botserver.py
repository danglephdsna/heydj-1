#! /usr/bin/python
# -*- coding: utf-8 -*- 

# slack integration
# https://github.com/lins05/slackbot
# pip install slackbot
from slackbot.bot import Bot, respond_to, listen_to, default_reply
from slackbot.utils import download_file, create_tmp_file

# for Slack bot token
import slackbot_settings

# datetime calculation
from datetime import datetime, timedelta
import argparse
import os, re, json
import requests

import time

from urllib.parse import urlparse, parse_qs


web_server="http://127.0.0.1:5000"
door_server="http://10.2.13.211:5000"

allowed_channel_list=['#music','#music_testing', 'DirectMessage']

def mpMsgHandler(mediaInfo):
    # mediaInfo=json.loads(jsonStr)
    # print (mediaInfo)
    replyMsg="""
        media['title']
        media['thumbs']
        media['duration']
        media['rating']
        media['addUser']
        media['addedDate']
    """
    return mediaInfo

def block_section(text, image_url=None, alt_text=None):
    retval=None
    if image_url is None or alt_text is None:
        retval={
        "type":"section",
            "text": {
                "type":"mrkdwn",
                "text":text,
            }
        }
    else:
        retval={
            "type":"section",
            "text": {
                "type":"mrkdwn",
                "text":text,
            },
            "accessory": {
                "type":"image",
                "image_url":image_url,
                "alt_text": alt_text
            }
        }
    return retval

def block_divider():
    return { "type": "divider" }

def block_context(text):
    return {
        "type": "context",
        "elements": [
            {
                "type": "mrkdwn",
                "text": text
            }
        ]
    }


def current_channel(message):
    try:
        channel = message.channel._client.channels[message.body['channel']]
        channel_name="#"+channel['name']
        return channel_name
    except KeyError:
        return 'DirectMessage'


def send_channel(message, blocks, text=None):
    url = 'https://slack.com/api/chat.postMessage'
    channel_name= current_channel(message)
    if channel_name is 'DirectMessage':
        channel_name=message._get_user_id()

    if blocks is None:
        payload={
            'channel': channel_name,
            'token': slackbot_settings.API_TOKEN, #Bot.slackbot_settings.API_TOKEN,
            'text': text,
            'as_user': True
        }
    else:
        payload={
            'channel': channel_name,
            'token': slackbot_settings.API_TOKEN, #Bot.slackbot_settings.API_TOKEN,
            'text': 'block_message',
            'as_user': True,
            'blocks': json.dumps(blocks)
        }

    retval = requests.post(url, params=payload)


@respond_to('hi')
def hi(message):
    message.reply('I can understand hi or HI')
    message.react('+1')


@listen_to("http(.*)", re.IGNORECASE)
@respond_to("http(.*)", re.IGNORECASE)
def bot_add(message, url=None):
    if current_channel(message) in allowed_channel_list:
        youtubeId=None
        ### Because of the bug of slackbot.bot library. it returns '<'+str+'>'.
        ### So I'm removing it.
        ### Also need add 'http' in front of url
        ### because library removes http(.*) from message.
        # url="http"+url[1:-1]
        url="http"+url.lstrip('<').rstrip('>')

        if url.startswith("http") >= 0:
            res=urlparse(url)
            if res.netloc == "www.youtube.com" or res.netloc == "youtube.com":
                # Url starts with www.youtube.com
                youtubeId=parse_qs(res.query)['v'][0]

            if res.netloc == "youtu.be":
                # Url starts with youtu.be
                youtubeId=res.path[1:]

        if youtubeId is None:
            message.reply("Sorry I only can understand youtube link")
        else:
            userid=message._get_user_id()
            username=message._client.users[userid]['name']

            response=requests.get(web_server+"/add", {'v':youtubeId, 'u':userid, 'n':username})
            mediaInfo=mpMsgHandler(response.json())
            if mediaInfo['server'] == 'success':
                msg= "Added: *{}* - {}\n".format(mediaInfo['title'], mediaInfo['duration'])
                response=requests.get(web_server+"/queue")
                mediaInfo=mpMsgHandler(response.json())
                msg+="Your song will be played after {} seconds".format(mediaInfo['remainingSec'])
            else:
                msg=mediaInfo['msg']
            message.reply(msg)

@listen_to('play')
@respond_to('play')
def bot_play(message):
    if current_channel(message) in allowed_channel_list:
        response=requests.get(web_server+"/play")
        mediaInfo=mpMsgHandler(response.json())
        if mediaInfo['status']=="nomusic":
            msg=mediaInfo['msg']
        else:
            msg= "Playing: *{}* - {} added by @{}\n".format(mediaInfo['title'], mediaInfo['duration'],'User')
            msg+=" Queue: {}".format(mediaInfo['currentIdx'])
        message.send(msg)

@listen_to("stop", re.IGNORECASE)
@respond_to("stop", re.IGNORECASE)
def bot_stop(message):
    if current_channel(message) in allowed_channel_list:
        response=requests.get(web_server+"/stop")
        mediaInfo=mpMsgHandler(response.json())

        msg= "Stoppd: *{}* - {}\n".format(mediaInfo['title'], mediaInfo['duration'])
        msg+=" Queue: {}".format(mediaInfo['currentIdx'])
        message.send(msg)

@listen_to("pause")
@respond_to("pause")
def bot_pause(message):
    if current_channel(message) in allowed_channel_list:
        response=requests.get(web_server+"/pause")
        mediaInfo=mpMsgHandler(response.json())

        msg= "Paused: *{}* - {}\n".format(mediaInfo['title'], mediaInfo['duration'])
        msg+=" Queue: {}".format(mediaInfo['currentIdx'])
        message.send(msg)

@listen_to("next")
@listen_to("skip")
@respond_to("next")
@respond_to("skip")
def bot_skip(message):
    if current_channel(message) in allowed_channel_list:
        userid=message._get_user_id()
        username=message._client.users[userid]['name']

        # print("userid:", userid, "username", username)

        response=requests.get(web_server+"/next", {'u':userid, 'n':username})

        mediaInfo=mpMsgHandler(response.json())
        if mediaInfo['server'] == 'success':
            msg="Next Up ({}) *{}* - {}\n".format(mediaInfo['currentIdx'], mediaInfo['title'], mediaInfo['duration'])
        else:
            msg=mediaInfo['msg']
        message.send(msg)

@listen_to("status")
@respond_to("status")
def bot_status(message):
    if current_channel(message) in allowed_channel_list:
        response=requests.get(web_server+"/status")
        mediaInfo=mpMsgHandler(response.json())
        msg=None
        try:
            playingAt=mediaInfo['playing_at']
        except:
            playingAt="Stopped"

        msg="{}: *{}* - {}\n".format(mediaInfo['status'], mediaInfo['title'], playingAt)
        msg+=" Queue: {}".format(mediaInfo['currentIdx'])

        if mediaInfo['volume'] < 80 :
            msg+= "\n:sound: *{}*\n".format(mediaInfo['volume'])
        else:
            msg+= "\n:loud_sound: *{}*\n".format(mediaInfo['volume'])
        message.send(msg)


@listen_to("list")
@respond_to("list")
def bot_queue(message):
    if current_channel(message) in allowed_channel_list:
        response=requests.get(web_server+"/queue")
        mediaInfo=mpMsgHandler(response.json())

        blocks=list()
        
        blocks.append(block_section("Here are upcomming music list."))
        blocks.append(block_divider())

        items=mediaInfo['items']

        count = 0
        for item in items:
            if count >= 10:
                break

            count+=1
            text=""
            text+=f"*{item['title']}* {item['duration']}\n"
            text+=f"youtu.be/{item['videoId']}\n"
            text+="Rate: {}\n".format(":star:"*int(item['rating']))
            text+=f"_request by *{item['addUser']}*_\n"

            blocks.append(block_section(text, item['thumbs'], item['title']))
            blocks.append(block_divider())




        text=f"Total *{count} of {len(items)}* items are listed"
        blocks.append(block_context(text))

        send_channel(message, blocks)


@listen_to("vol(.*)")
@respond_to("vol(.*)")
def bot_volume(message, volume=None):
    if current_channel(message) in allowed_channel_list:
        # volume=int(volume[1:-1])
        if volume not in [None, ""]:
            print("rx_volume", int(volume))
            response=requests.get(web_server+"/volume", {'l':int(volume)} )

        response=requests.get(web_server+"/volume")
        # response=requests.get(server+"/volume")
        mediaInfo=mpMsgHandler(response.json())
        if mediaInfo['volume'] < 80 :
            msg= ":sound: *{}*\n".format(mediaInfo['volume'])
        else:
            msg= ":loud_sound: *{}*\n".format(mediaInfo['volume'])
        message.send(msg)


@listen_to("bug")
@respond_to("bug")
def bot_bug(message):
    if current_channel(message) in allowed_channel_list:
        msg= "If you find a bug, please fix it and send me a pull request.\n"
        msg+="*Jira*: https://snaglobal.atlassian.net/secure/RapidBoard.jspa?projectKey=HDJ\n"
        msg+="*BitBucket*: https://bitbucket.org/1kko/heydj/"
        message.send(msg)


@respond_to("ping")
def ping(message=None):
    response=requests.get(web_server+"/uptime")
    mediaInfo=mpMsgHandler(response.json())
    if message is None:
        print(mediaInfo['msg'])
    else:
        message.send("*pong* I'm up and running for "+mediaInfo['msg'])


@listen_to("opendoor")
@respond_to("opendoor")
def opendoor(message=None):
    if current_channel(message) in allowed_channel_list+['#0_team_general']:
        response=requests.get(door_server+"/opendoor")
        msg=response.json()['status']
        message.send(msg)


@listen_to("showdoor")
@respond_to("showdoor")
def take_picture(message):
    if current_channel(message) in allowed_channel_list:
        fname=datetime.now().strftime("%Y%m%d_%H%M%S")+".jpg"
        with create_tmp_file() as tmpf:
            download_file(web_server+"/showdoor",tmpf)
            message.channel.upload_file(fname, tmpf, "picture of front door")


@listen_to("setindex(.*)")
@respond_to("setindex(.*)")
def set_index(message, index):
    if current_channel(message) in allowed_channel_list:
        if index not in [None, ""]:
            response=requests.get(web_server+"/setindex", {'i':int(index)})
        response=requests.get(web_server+"/setindex")
        mediaInfo=mpMsgHandler(response.json())
        msg="index set: {}".format(mediaInfo['index'])
        message.send(msg)


@respond_to("search(.*)")
@listen_to("search(.*)")
def search(message, query):
    print(query.strip())
    thisChannel=current_channel(message)
    if thisChannel in allowed_channel_list:
        
        response=requests.get(web_server+"/search", {'q':query.strip()})
        if len(response.content) is not 0 and response is not None:
            print("content Length: ",len(response.content))
            
            blocks=list()
            
            blocks.append(block_section("Here are list for your search results."))
            blocks.append(block_divider())

            items=json.loads(response.content)

            for item in items:
                text=""
                text+=f"*{item['title']}* {item['duration']}\n"
                text+=f":link: {item['link']}\n"
                text+=f"{item['desc']}\n"

                blocks.append(block_section(text, item['thumbs'], item['title']))
                blocks.append(block_divider())

            text=f"Total *{len(items)}* items are listed"
            blocks.append(block_context(text))

            print(blocks)

        if thisChannel is 'DirectMessage':
            send_channel(message, blocks)


@listen_to("help")
@respond_to("help")
def bot_help(message):
    if current_channel(message) in allowed_channel_list:
        msg="Just give me a youtube link then I'll add to the playlist.\n"
        msg+="You can also *play*, *pause*, *stop*, *skip*, *search {keywords}*, *status*, *list*, *vol {0~100}*, *help*, *bug*"
        message.send(msg)


if __name__ == "__main__":
    print("starting bot")
    parser = argparse.ArgumentParser()
    parser.add_argument('-q','--quick', action='store_true', help="start without delay")
    args = parser.parse_args()

    if args.quick is True:
        pass
    else:
        time.sleep(10) # lazy loading. give time webserver.py to start first
    heyDJ=Bot()
    ping()
    print("I'm up and running")
    heyDJ.run()
