#! /usr/bin/python
# -*- coding: utf-8 -*-

# 1. install dependency.
# sudo apt install vlc
# pip install python-vlc pafy youtube-dl flask opencv-contrib-python requests youtube-search 

# 2. go to https://www.videolan.org/vlc/ download and install VLC Player.
# requires to set environment variable for VLC Library 


# saving and managing playlist
import sqlite3
# Flask Web Service
from flask import Flask, make_response, request, redirect, send_file, stream_with_context, Response
# Youtube
# pip install pafy youtube-dl 
import pafy
import vlc

# http resquests
import requests

# pip install youtube-search
from youtube_search import YoutubeSearch

# opencv
import cv2

# getting slackbot settings
import slackbot_settings


import psutil
import time
import json
import sys, os
# datetime calculation
from datetime import datetime, timedelta

TIME_DURATION_UNITS = (
    ('week', 60*60*24*7),
    ('day', 60*60*24),
    ('hour', 60*60),
    ('min', 60),
    ('sec', 1)
)



def human_time_duration(seconds):
    if seconds == 0:
        return 'inf'
    parts = []
    for unit, div in TIME_DURATION_UNITS:
        amount, seconds = divmod(int(seconds), div)
        if amount > 0:
            parts.append('{} {}{}'.format(amount, unit, "" if amount == 1 else "s"))
    return ', '.join(parts)

class myPlayerClass():

    def __init__(self):
        self.dbfile = os.path.dirname(
            os.path.abspath(__file__)) + "/playlist.db"
        self.conn = self.connectDB()

        pafy.set_api_key(slackbot_settings.YOUTUBE_API_KEY)

        self.initMP()

        # User limit
        self.maxReqCount = 10
        self.maxReqTime = timedelta(minutes=60)

        try:
            self.currentIdx = self.getCurrentIdx()
            self.videoId = self.getVideoIdByIdx(self.currentIdx)
            self.mediaInfo = self.getMediaInfo(self.videoId)
        except:
            self.currentIdx = None
            self.videoId = None
            self.mediaInfo = None

    def initMP(self):
        #self.vlcInstance = vlc.Instance("--run-time=10") # run for 10 seconds on each media item
        self.vlcInstance = vlc.Instance()
        self.mp = self.vlcInstance.media_player_new()
        self.mp.audio_set_volume(80)

        self.mp_em = self.mp.event_manager()
        self.mp_em.event_attach(vlc.EventType.MediaPlayerEndReached, self.cb)
        self.mp_em.event_attach(
            vlc.EventType.MediaListPlayerNextItemSet, self.cb)

    @vlc.callbackmethod
    def cb(self, event):
        print("callback:", event.type)
        if event.type in [vlc.State.Ended]:
            #self.currentIdx += 1
            # self.setCurrentIdx(self.currentIdx)
            pass

        if event.type in [vlc.EventType.MediaPlayerMediaChanged]:
            pass

        elif event.type in [vlc.EventType.MediaListItemAdded]:
            # print(">>>>", self.currentIdx)
            # if self.checkNextSongAvailable(self.currentIdx) is True:

            # self.mlp.set_media_list(self.ml)
            if self.mp.get_state() not in [vlc.State.Playing]:
                print("Event fired: vlc state not playing")
                # self.play()

        elif event.type in [vlc.EventType.MediaPlayerEndReached]:
            if self.checkNextSongAvailable(self.currentIdx) is False:
                self.currentIdx += 1
                self.setCurrentIdx(self.currentIdx)
                print("Player Stopped")
            else:
                print("getting next song")
                self.currentIdx += 1
                self.setCurrentIdx(self.currentIdx)
                self.videoId = self.getVideoIdByIdx(self.currentIdx)
                self.mediaInfo = self.getMediaInfo(self.videoId)
                self.initMP()
                self.mp.set_mrl(self.mediaInfo['mrl'])
                self.mp.play()
        return

    def connectDB(self):
        conn = None
        try:
            conn = sqlite3.connect(self.dbfile, detect_types=sqlite3.PARSE_DECLTYPES |
                                   sqlite3.PARSE_COLNAMES, check_same_thread=False)
            conn.row_factory = sqlite3.Row

        except Error as e:
            print(e)
        return conn

    def executeSQL(self, sql):
        try:
            cursor = self.conn.cursor()
            cursor.execute(sql)
            self.conn.commit()
        except Error as e:
            print(e)

    def runOnce(self, param=False):
        if param is True:
            print(self.conn)
            if self.conn is not None:
                print("!!!WARNING!!! Closing Database...")
                self.conn.close()

            print("os.path.isfile(self.dbfile)", os.path.isfile(self.dbfile))
            if os.path.isfile(self.dbfile):
                print("!!!WARNING!!! Removing Database...")
                os.remove(self.dbfile)

            print("!!!WARNING!!! Re-Initilizing Database...")
            self.conn = self.connectDB()

            with open(os.path.dirname(
                            os.path.abspath(__file__)) + '/initdb.sql', 'r', encoding='utf-8') as fp:
                cursor = self.conn.cursor()
                cursor.executescript(fp.read())
                self.conn.commit()
                self.conn.close()
            print("Initialization Complete.")
            print("Please set runOnce(False) in __main__ to correctly run server.")
            shutdown = request.environ.get('werkzeug.server.shutdown')
            if func is None:
                raise RuntimeError('Not running with the Werkzeug Server')
            shutdown()

            sys.exit()

    def getMediaInfo(self, videoId):
        retval = None
        if videoId is not None:
            url = "https://www.youtube.com/watch?v="+videoId
            print("url",url)
            media = pafy.new(url)
            mrl = media.getbestaudio().url
            #mrl = media.getbest().url

            retval = {
                "url": url,
                "mrl": mrl,
                "videoId": media.videoid,
                "title": media.title,
                "thumbs": media.bigthumb,
                "duration": media.duration,
                "rating": media.rating,
            }
        self.mediaInfo = retval
        return retval

    def getVideoIdByIdx(self, playlistIdx):
        # print ("getting IDX :%s" %playlistIdx)
        cursor = self.conn.cursor()
        cursor.execute("""
            SELECT videoId FROM playlist WHERE id=%s;
            """ % playlistIdx)
        try:
            videoId = cursor.fetchone()['videoId']
        except:
            videoId = None
        return videoId

    def getMaxIdx(self):
        cursor = self.conn.cursor()
        cursor.execute("""
            SELECT id from playlist ORDER BY id DESC LIMIT 0,1;""")
        maxIdx = cursor.fetchone()['id']
        if maxIdx is None:
            maxIdx=0
        return maxIdx

    def getCurrentIdx(self):
        maxIdx = self.getMaxIdx()
        cursor = self.conn.cursor()
        cursor.execute("""
            SELECT value AS idx FROM variables WHERE key='currentIdx';""")
        row = cursor.fetchone()

        if row['idx'] is None:
            idx = 1
        else:
            idx = int(row['idx'])
            if idx > maxIdx:
                idx = maxIdx
        self.currentIdx = idx
        print("getting current Index - currentIdx:", idx)

        return idx

    def setCurrentIdx(self, idx):
        maxIdx = self.getMaxIdx()
        retval = False
        try:
            idx = int(idx)  # variables are always string
            if idx > maxIdx:
                idx = maxIdx
            self.currentIdx = idx
    
            cursor = self.conn.cursor()
            cursor.execute("""
                UPDATE variables SET value = '%s' WHERE key='currentIdx';
            """ % str(idx))
            self.conn.commit()
            retval = True
        except:
            return retval
        return retval

    def add(self, videoId, userId, userName):
        retval = self.getMediaInfo(videoId)
        retval['addDate'] = datetime.now()
        retval['result'] = "added"
        retval['addUser'] = userId
        retval['userName'] = userName
        retval['server'] = 'fail'

        # check `users` table with userId.
        proceed, retval['msg'] = self.checkUser(userId, userName)
        # if update date and count is > 10 then cancel the operation.

        if proceed is True:

            cursor = self.conn.cursor()
            cursor.execute("""
                INSERT INTO 'playlist' (
                    videoId, addDate, title, thumbs, duration, rating, addUser
                )
                VALUES (
                    :videoId, :addDate, :title, :thumbs, :duration, :rating, :addUser
                );
                """, retval)
            self.conn.commit()
            retval['server'] = 'success'
            print("user {} added music: {}".format(retval['userName'],retval['title']))
            if self.checkNextSongAvailable(self.currentIdx) is False:
                self.setCurrentIdx(self.currentIdx+1)

        # cursor.execute("""
        #     /* if user doesn't exist in users table add user to `users`
        #     if user exist update count and date.
        #     */
        #     """)
        print("current mediaplayer status: mp.get_state()", self.mp.get_state())

        return retval

    def status(self):
        retval = self.mediaInfo
        if retval == None:
            self.currentIdx = self.getCurrentIdx()
            videoId = self.getVideoIdByIdx(self.currentIdx)
            retval = self.getMediaInfo(videoId)
        retval['currentIdx'] = self.currentIdx
        retval['volume'] = self.mp.audio_get_volume()

        mpState = None
        if self.mp is not None:
            mpState = self.mp.get_state()
        if mpState in [vlc.State.Playing, vlc.State.Paused]:
            current_mm, current_ss = divmod(
                self.mp.get_length()*self.mp.get_position()/1000, 60)
            current_hh, current_mm = divmod(current_mm, 60)
            retval['playing_at'] = "%02d:%02d:%02d" % (
                int(current_hh), int(current_mm), int(current_ss))
            retval['status'] = str(mpState).replace("State.", "").lower()

        retval['status'] = str(mpState).replace("State.", "").lower()
        retval['server'] = 'success'
        return retval

    def queue(self):
        retval = None
        cursor = self.conn.cursor()

        result = cursor.execute("""
            SELECT
                playlist.id,
                addDate,
                videoId, 
                title, 
                thumbs, 
                time(duration) AS duration, 
                rating, 
                addUser AS userId, 
                users.displayName AS adduser
            FROM playlist LEFT JOIN users
            WHERE playlist.id > {} AND users.id=addUser
            ORDER BY playlist.id ASC;""".format(self.currentIdx))

            # SELECT id, videoId, title, thumbs, time(duration) AS duration, rating, addUser
            # FROM playlist
            # WHERE id > {}
            # ORDER BY id ASC;""".format(self.currentIdx))
        items = []

        remainingSec = 0
        for row in result:
            items.append({
                'id': row['id'],
                'videoId': row['videoId'],
                'title': row['title'],
                'thumbs': row['thumbs'],
                'duration': row['duration'],
                'rating': row['rating'],
                'addUser': row['adduser']
            })
            t = datetime.strptime(row['duration'], "%H:%M:%S")
            remainingSec += (t.second*1)+(t.minute*60)+(t.hour*60*60)

        retval = {"items": items, "remainingSec": remainingSec}
        retval['server'] = 'success'
        return retval

    def skip(self, event=None, status=1, userId=None, userName=None):
        # print ("Playing Next media")
        retval = self.status()
        if userId is None:
            proceed = True
        else:
            proceed, retval['msg'] = self.checkUser(userId, userName)

        if proceed is False:
            retval['server'] = 'fail'
        else:
            if self.currentIdx == None:
                self.currentIdx = self.getCurrentIdx()
            else:
                if self.checkNextSongAvailable(self.currentIdx) is False:
                    self.mp.stop()
                    return self.status()

            print("before set+1: self.currentIdx",self.currentIdx)
            self.setCurrentIdx(self.currentIdx+1)
            self.currentIdx = self.getCurrentIdx()
            print("after  set+1: self.currentIdx",self.currentIdx)

            self.videoId = self.getVideoIdByIdx(self.currentIdx)
            self.mediaInfo = self.getMediaInfo(self.videoId)

            print("before stop: self.currentIdx",self.currentIdx)
            self.mp.stop()
            print("after  stop: self.currentIdx",self.currentIdx)
            # self.initMP()
            self.mp.set_mrl(self.mediaInfo['mrl'])
            print("before play: self.currentIdx",self.currentIdx)
            self.mp.play()
            print("after  play: self.currentIdx",self.currentIdx)

            retval = self.status()
            # print (self.mediaInfo)
        print(retval)
        return retval

    def pause(self):
        self.mp.pause()
        return self.status()

    def stop(self):
        self.mp.stop()
        return self.status()

    def play(self):
        retval = {"status": "nomusic",
                  "msg": "There's nothing to play. Please add more songs."}
        print("self.currentIdx", self.currentIdx)
        print("self.mp.get_state:", self.mp.get_state())
        if self.mp.get_state() not in [vlc.State.Playing]:
            self.initMP()
            if self.checkNextSongAvailable(self.currentIdx):
                if self.mp.get_state() in [vlc.State.NothingSpecial]:
                    self.currentIdx = self.getCurrentIdx()+1
                    print("currentIdx", self.currentIdx)
                self.videoId = self.getVideoIdByIdx(self.currentIdx)
                #print ("videoId",self.videoId)
                self.mediaInfo = self.getMediaInfo(self.videoId)
                #print ("mediaInfo", self.mediaInfo)
                self.mp.set_mrl(self.mediaInfo['mrl'])
                #print ("addMedia:", self.mediaInfo['mrl'])

                #print ("start playing mp.play()")
                try:
                    self.mp.play()
                    # if self.ml.index_of_item(self.mlp) < 0:
                    #    self.mlp.play_item_at_index(1)
                    # else:
                    #    self.mlp.play()
                except:
                    print("Cannot play this song")
                    self.skip()
                retval = self.status()
                print("title", retval['title'])
            else:
                pass

        return retval

    def volume(self, volume=None):
        #print("I'm in volume", volume)
        print("volume", volume)
        #print("dir(volume)", dir(volume))
        if volume not in [None, ""]:
            volume = int(volume)
            if volume >= 100:
                volume = 100
            elif volume <= 0:
                volume = 0
            self.mp.audio_set_volume(volume)
        return myPlayer.status()


    def checkUser(self, userId, userName=None):
        cursor = self.conn.cursor()
        result = None

        userInfo = {
            'id': userId,
            'lastActionTime': datetime.now(),
            'actionCount': 0,
            'displayName': userName,
        }

        # try to get the current user's info.
        sql = """
            SELECT id, lastActionTime, actionCount, displayName
            FROM users
            WHERE id = '%s' ORDER BY lastActionTime DESC LIMIT 0,1;""" % userInfo['id']

        cursor.execute(sql)
        row = cursor.fetchone()

        # if user does not exists, then create current user's info.
        if row is None:
            cursor.execute("""
                INSERT INTO 'users' (
                    id, lastActionTime, actionCount, displayName
                )
                VALUES (
                    :id, :lastActionTime, :actionCount, :displayName
                );
            """, userInfo)
            self.conn.commit()
        else:
            userInfo['lastActionTime'] = row['lastActionTime']
            userInfo['actionCount'] = row['actionCount']
            userInfo['displayName'] = row['displayName']

        timelimit = userInfo['lastActionTime'] + self.maxReqTime

        # now update user if user action count and date is resonable.
        
        if True: # removed restriction and bypass for a while
        # if userInfo['actionCount'] >= self.maxReqCount and datetime.now() < timelimit:
        #     msg = "Sorry {}, you cannot perform this action due to too many request\n".format(
        #         userInfo['displayName'])
        #     msg += "Please try again after {}".format(timelimit)
        #     return False, msg
        # else:
            if userInfo['actionCount'] >= self.maxReqCount:
                userInfo['actionCount'] = 0

            userInfo['actionCount'] += 1
            userInfo['lastActionTime'] = datetime.now()

            cursor.execute("""
                UPDATE users SET lastActionTime=?, actionCount=?  WHERE id=?;""",
                           (userInfo['lastActionTime'],
                            userInfo['actionCount'], userInfo['id'])
                           )
            self.conn.commit()

            msg = "Your have %d request available." % (
                self.maxReqCount - userInfo['actionCount'])
            return True, msg

    def checkNextSongAvailable(self, idx):
        cursor = self.conn.cursor()
        print('checkNextSongAvailable idx:', idx)
        sql = """
            SELECT count(playlist.id) AS count
            FROM playlist
            WHERE playlist.id > {};""".format(idx)
        print("sql", sql)
        cursor.execute(sql)

        nextExist = cursor.fetchone()['count'] > 0
        # print(">>>", nextExist)
        return nextExist


app = Flask(__name__)


@app.route("/")
def home():
    return json.dumps({"title": "Welcome to Hello DJ"})


@app.route("/play")
def http_play():
    return myPlayer.play()


@app.route("/stop")
def http_stop():
    myPlayer.stop()
    return json.dumps(myPlayer.status())


@app.route("/pause")
def http_pause():
    # myPlayer.pause()
    # return make_response("OK",200)
    return myPlayer.pause()


@app.route("/next", methods=['GET'])
def http_skip():
    # myPlayer.skip()
    userId = request.args.get('u')
    userName = request.args.get('n')
    return myPlayer.skip(userId=userId, userName=userName)


@app.route("/status")
def http_status():
    return myPlayer.status()


@app.route("/queue")
def http_queue():
    playlist = myPlayer.queue()
    retval = json.dumps(playlist)
    return retval


@app.route("/volume", methods=['GET'])
def http_setVolume():
    try:
        vol = request.args.get('l')
    except:
        vol = None
    return myPlayer.volume(vol)
    # return myPlayer.status()


@app.route("/add", methods=['GET'])
def http_add():
    videoId = request.args.get('v')
    userId = request.args.get('u')
    userName = request.args.get('n')
    return myPlayer.add(videoId, userId, userName)


@app.route("/uptime")
def http_ping():
    print("ping post")
    uptime = time.time() - psutil.boot_time()

    # retval
    return {'server': 'success', 'msg': human_time_duration(uptime), 'time':uptime}


@app.route("/showdoor")
def http_showdoor():
    fnamePrefix = "door_"
    fnamePostfix = ".jpg"
    print("making connection video")
    cap = cv2.VideoCapture("rtsp://test:Camera123@10.2.11.52")
    print("reading frame")
    retval, frame = cap.read()
    now = datetime.now()
    fileName = fnamePrefix+now.strftime("%Y%m%d_%H%M%S")+fnamePostfix
    filePath = os.path.join(os.getcwd(), fileName)
    # print(filePath)
    # print("result=",retval)

    # before write delete all jpg files with prefix
    for fname in os.listdir(os.getcwd()):
        if fname.startswith(fnamePrefix) and fname.endswith(fnamePostfix):
            os.remove(os.path.join(os.getcwd(), fname))

    cv2.imwrite(filePath, frame)
    cap.release()
    cv2.destroyAllWindows()
    if retval is True:
        return send_file(filePath, attachment_filename=fileName)
    return retval


@app.route("/bellrang")
def http_bellring():
    fnamePrefix = "bell_"
    fnamePostfix = ".jpg"
    response = requests.get("http://localhost:5000" +
                            "/showdoor")  # , chunk_size=512)

    now = datetime.now()
    fileName = fnamePrefix+now.strftime("%Y%m%d_%H%M%S")+fnamePostfix
    filePath = os.path.join(os.getcwd(), fileName)

    # before write delete all jpg files with prefix
    for fname in os.listdir(os.getcwd()):
        if fname.startswith(fnamePrefix) and fname.endswith(fnamePostfix):
            os.remove(os.path.join(os.getcwd(), fname))

    with open(filePath, 'wb') as f:
        for chunk in response.iter_content():
            f.write(chunk)

    print("file exists", os.path.isfile(filePath))

    # sending to slack
    url = 'https://slack.com/api/files.upload'
    retval = None
    with open(filePath, 'rb') as fp:
        my_file = {'file': (filePath, fp, 'jpg')}
        payload = {'initial_comment': 'someone is at the door',
                   'channels': ['#0_team_general'],
                   'token': slackbot_settings.API_TOKEN
                   }
        print("payload", payload)
        retval = requests.post(url, params=payload, files=my_file)
        print("post done")
    print(retval.content)
    return json.dumps({"status": "ok"})

@app.route("/search", methods=['GET'])
def http_search():
    results={}
    q = request.args.get('q')
    if q is not None:
        results = YoutubeSearch(q, max_results=10).to_dict()
        """ results example 
        [{'title': '【PV】【血界战线 OP】【Hello,world!】 无字幕版', 'link': '/watch?v=DJsFPu_unlE', 'id': 'DJsFPu_unlE'}, {'title': '\u200b[팀 아리아] 혈계전선 여는 노래 - Hello,world! (풀버전)', 'link': '/watch?v=WpZ5Qam-UJA', 'id': 'WpZ5Qam-UJA'}...]
        """
    return json.dumps(results)


@app.route("/setindex", methods=['GET'])
def http_setIndex():
    try:
        idx = int(request.args.get('i'))
    except:
        idx = myPlayer.getCurrentIdx()

    if myPlayer.setCurrentIdx(idx):
        return json.dumps({'index': idx})


time.sleep(2)
myPlayer = myPlayerClass()
myPlayer.runOnce(False)

if __name__ == "__main__":
    app.run(debug=True, threaded=True, host="0.0.0.0")
